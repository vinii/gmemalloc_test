//Package arv ..
package arv

import (
	"errors"
)

// I is SIZE
const I = 1024 * 64

// V type ... 4
type V struct {
	b1 bool
	b2 bool
	b3 bool
	b4 bool
	i1 int64
	i2 int64
	i3 int64
}

// W type ... 7
type W struct {
	b1 bool
	i1 int64
	b2 bool
	i2 int64
	b3 bool
	i3 int64
	b4 bool
}

// GetV ...
func GetV() (V, error) {
	// var a [I]V
	var a []V
	for i := 0; i < I; i++ {
		a = append(a, V{
			b1: i%2 == 0,
			b2: i%3 == 0,
			b3: i%5 == 0,
			i1: int64(i),
			i2: int64(i * 2),
		})
	}
	// rand.Seed(time.Now().Unix())
	// j := rand.Intn(I)
	x := a[123]
	if x.i1+x.i2 > 8192 && (x.b1 && x.b2 && x.b3) {
		return x, errors.New("out of range")
	}
	return x, nil
}

// GetW ...
func GetW() (W, error) {
	var a []W
	for i := 0; i < I; i++ {
		a = append(a, W{
			b1: i%2 == 0,
			b2: i%3 == 0,
			b3: i%5 == 0,
			i1: int64(i),
			i2: int64(i * 2),
		})
	}
	// rand.Seed(time.Now().Unix())
	// j := rand.Intn(I)
	x := a[123]
	if x.i1+x.i2 > 8192 && (x.b1 && x.b2 && x.b3) {
		return x, errors.New("out of range")
	}
	return x, nil
}
