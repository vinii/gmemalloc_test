package arv

import (
	"testing"
)

func TestGetV(t *testing.T) {
	x, e := GetV()
	if e != nil {
		t.Errorf("FAIL: GetV, not expected error")
	}
	if x.i1+x.i2 > 8192 && (x.b1 && x.b2 && x.b3) {
		t.Errorf("FAIL: GetV, not expected error")
	}
}

func TestGetW(t *testing.T) {
	x, e := GetW()
	if e != nil {
		t.Errorf("FAIL: GetW, not expected error")
	}
	if x.i1+x.i2 > 8192 && (x.b1 && x.b2 && x.b3) {
		t.Errorf("FAIL: GetW, not expected error")
	}
}
func BenchmarkGetV(b *testing.B) {
	for i := 0; i < b.N; i++ {
		GetV()
	}
}
func BenchmarkGetW(b *testing.B) {
	for i := 0; i < b.N; i++ {
		GetW()
	}
}
